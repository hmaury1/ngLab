import { Component } from '@angular/core';
import { AppService } from './app.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  
  title = 'app lab 1';
  calc = 0;
  value1 = 0;
  value2 = 0;
  value3 = 0;

  constructor (private appService: AppService) { 

  }

  ngOnInit() {
  	this. calc = this.appService.calculate(this.value1,this.value2);
  }

  calculate = () => {
  	this. calc = this.appService.calculate(this.value1,this.value2);
  }
}
